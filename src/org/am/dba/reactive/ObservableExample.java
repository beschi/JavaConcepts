package org.am.dba.reactive;

import static java.util.stream.Collectors.toList;


import java.util.List;
import java.util.stream.Stream;

import io.reactivex.Observable;

/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 2, 2017
 * @purpose Example of Reactive Programming, list of values propagated to all interested parties(subscribes)
 */
public class ObservableExample  {

  public static void main(String... arg0) throws Exception {
	System.out.println("Running project");
	
	 //Arrays.asList(1,2,3,4,5);
	//Infinite streams can be created but limited to 10 using limit(n)
	List<Integer> list = Stream.iterate(1, e->e+1).limit(100).collect(toList());
	
	//attaching an observable to source/producer, here it is list
	Observable<Object> observable = Observable.fromArray(list.toArray());
	
	//Consumer suscribe(Action1<T>, Action1<Throwable>,Action0) 
	observable.subscribe((val)->System.out.println(val),
						 (err)->System.out.println(err),
						 ()->System.out.println("Completed Processing"));
	//Method call
	/*observable.subscribe(System.out::println, 
						 System.err::println, 
						 ()->System.out.println("Completed Processing"));*/

	//Infinite Stream Example
	/*Stream.iterate(1, e->e+1)
	      .forEach(System.out::println);*/
	      
  }

}
