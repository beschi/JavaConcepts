package org.am.dba.reactive;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observables.ConnectableObservable;

/**
 * 
 * @author D Beschi Antony
 * @email beschi.agent@gmail.com
 * @created on Jun 2, 2017
 * @purpose Demonstrates the Reactive Sum, for example in MS Excel, sum(a+b) in
 *          cell c changes, while the values in cell a or b changes.
 */
public class ReactiveExample {
  public static void main(String... strings) {
	ConnectableObservable<String> connectableObservable = from(System.in);
	Observable<Double> a = varObservable("a", connectableObservable);
	Observable<Double> b = varObservable("b", connectableObservable);
	connectableObservable.connect();

  }

  static ConnectableObservable<String> from(final InputStream stream) {
	return from(new BufferedReader(new InputStreamReader(stream)));
  }

  static ConnectableObservable<String> from(final BufferedReader reader){

	//Creating an observable and publish it
	return (ConnectableObservable<String>) Observable.create(new ObservableOnSubscribe<String>() {

	  @Override
	  public void subscribe(ObservableEmitter<String> oe) throws Exception {
		if(oe.isDisposed()){
		  return;
		}
		try{
		  String line = "";
		  while(!oe.isDisposed() && (line = reader.readLine()) != null){
			if(line ==  null || line.equals("exit")){
			  break;
			} 
			oe.onNext(line);
		  }
		}catch(Exception e){
		  oe.onError(e);
		}
		if(!oe.isDisposed()){
		  oe.onComplete();
		}
	  }
	}).publish();
  }
  
  static Observable<Double> varObservable(String name, Observable<String> input){
	final Pattern pattern = Pattern.compile("^"+name +"[:|=](-?\\d+\\.?\\d*)$");
/*	input.subscribe((str)->{
	  System.out.println(str);
	  Matcher matcher = pattern.matcher(str);
	  System.out.println(matcher.matches());});*/
	System.out.println("I am in");
	return input.map(new Function<String, Matcher>() {
	  
	  @Override
	  public Matcher apply(String var) throws Exception {
		System.out.println("Input String ::==>"+var);
		return pattern.matcher(var);
	  }
	}).filter(new Predicate<Matcher>() {
	  @Override
	  public boolean test(Matcher matcher) throws Exception {
		System.out.println("Group 1 --"+matcher.group(1));
		return matcher.matches() && matcher.group(1) != null;
	  }
	}).map(new Function<Matcher, Double>() {
	  @Override
	  public Double apply(Matcher matcher) throws Exception {
		return Double.parseDouble(matcher.group(1));
	  }
	});
  }
}
