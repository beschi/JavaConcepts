package org.am.dba.designPattern.factory;
/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose    An interface/abstract declares the contract, one are many concreate classes implements that contract. 
 * And Factory class creates the instance of concrete classes based the input that we provide to factory method, 
 * Factory instance decide which instance to create 
 */
public class FactoryPattern {
  public static void main(String[] args) {
	Shape circle = ShapeFactory.getShape("CIRCLE");
	circle.draw();
	
	Shape square = ShapeFactory.getShape("SQUARE");
	square.draw();
  }
}
