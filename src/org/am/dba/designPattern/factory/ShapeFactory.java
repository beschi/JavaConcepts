package org.am.dba.designPattern.factory;

/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose
 */
public class ShapeFactory {
  /**
   * Provides shape object based on the type argument
   * @return
   */
  public static Shape getShape(String type){
	switch(type){
	case "CIRCLE":
	  	return new Circle();
	case "RECTANGLE":
	  	return new Rectangle();
	case "SQUARE":
	    return new Square();
	default:
	  	return null;
	}
  }
}
