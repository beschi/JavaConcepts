package org.am.dba.designPattern.factory;

public class Square implements Shape {

  @Override
  public void draw() {
	System.out.println("Draw Square");
  }

}
