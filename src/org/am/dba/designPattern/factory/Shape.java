package org.am.dba.designPattern.factory;

public interface Shape {
  
  public void draw();

}
