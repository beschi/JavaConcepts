package org.am.dba.designPattern.abstractfactory;

public class MacOSCheckBox implements ICheckbox {

  @Override
  public void paint() {
	System.out.println("Painting MacOS CheckBox");
  }

}
