package org.am.dba.designPattern.abstractfactory;

public class WindowsCheckBox implements ICheckbox {

  @Override
  public void paint() {
	System.out.println("Painting Windows Checkbox");
  }

}
