package org.am.dba.designPattern.abstractfactory;

public class MacOSButton implements IButton {

  @Override
  public void paint() {
	System.out.println("Painting MacOS Button");
  }

}
