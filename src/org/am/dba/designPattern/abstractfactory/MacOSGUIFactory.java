package org.am.dba.designPattern.abstractfactory;

public class MacOSGUIFactory extends GUIFactory {

  public MacOSButton createButton() {
 	return new MacOSButton();
   }

   @Override
   public MacOSCheckBox createCheckBox() {
 	return new MacOSCheckBox();
   }
 

}
