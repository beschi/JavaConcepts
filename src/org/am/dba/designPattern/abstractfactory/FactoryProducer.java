package org.am.dba.designPattern.abstractfactory;

public class FactoryProducer {
  
  public static GUIFactory getFactory(String os){
	switch(os){
	case "win":
	  	return new WindowsGUIFactory();
	case "mac":
	  	return new MacOSGUIFactory();
	default:
	  	return null;
	}
  }

}
