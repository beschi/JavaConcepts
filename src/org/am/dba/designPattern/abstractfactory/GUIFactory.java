/**
 * 
 */
package org.am.dba.designPattern.abstractfactory;

/**
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose 
 */
public abstract class GUIFactory {
  public abstract IButton createButton();
  public abstract ICheckbox createCheckBox();
}
