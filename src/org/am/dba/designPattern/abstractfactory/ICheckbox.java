package org.am.dba.designPattern.abstractfactory;

public interface ICheckbox {
  void paint();
}
