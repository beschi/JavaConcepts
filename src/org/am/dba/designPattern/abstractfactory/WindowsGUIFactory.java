package org.am.dba.designPattern.abstractfactory;

public class WindowsGUIFactory extends GUIFactory {

  @Override
  public IButton createButton() {
	return new WindowsButton();
  }

  @Override
  public ICheckbox createCheckBox() {
	return new WindowsCheckBox();
  }

}
