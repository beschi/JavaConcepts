package org.am.dba.designPattern.abstractfactory;

public class Application {
  public static void main(String[] args) {
	GUIFactory factory = FactoryProducer.getFactory("win");
	IButton button = factory.createButton();
	button.paint();
	
	GUIFactory factory1 = FactoryProducer.getFactory("mac");
	IButton button1 = factory1.createButton();
	button1.paint();
  }
}
