package org.am.dba.designPattern.abstractfactory;

public interface IButton {
  void paint();
}
