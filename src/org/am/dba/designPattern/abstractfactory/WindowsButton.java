package org.am.dba.designPattern.abstractfactory;

public class WindowsButton implements IButton {

  @Override
  public void paint() {
	System.out.println("Painting Windows Button");
  }

}
