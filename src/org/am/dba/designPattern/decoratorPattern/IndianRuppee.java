package org.am.dba.designPattern.decoratorPattern;
/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose A CONCRETE COMPONET
 */
public class IndianRuppee extends Currency {
  double v;
  private String description;

  public IndianRuppee() {
	description = "Indian Rupee";
  }

  @Override
  public double cost(double value) {
	v = value;
	return v;
  }

  @Override
  public String getCurrencyDescription() {
	return description;
  }

}
