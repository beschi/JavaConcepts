package org.am.dba.designPattern.decoratorPattern;

/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose A CONCRETE COMPONENT IN DECORATOR PATTER
 */
public class UsDollor extends Currency {
  double v;
  String description;
  
  public UsDollor() {
	description="US Dollor";
  }

  @Override
  public double cost(double value) {
	return 0;
  }

}
