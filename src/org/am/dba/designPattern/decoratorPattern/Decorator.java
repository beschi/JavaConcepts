package org.am.dba.designPattern.decoratorPattern;
/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose A DECORATOR WHICH EXTENDS COMPONENT
 */
public abstract class Decorator extends Currency {
  public abstract String getDescription();
}
