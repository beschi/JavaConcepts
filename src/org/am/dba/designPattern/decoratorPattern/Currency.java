package org.am.dba.designPattern.decoratorPattern;
/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 1, 2017
 * @purpose COMPONENT, STARTING POINT FOR THE "DECORATOR PATTERN " EXAMPLE
 */
public abstract class Currency {
  private String description = "Unknown Currency";
  
  public String getCurrencyDescription(){
	return description;
  }
  
  public abstract double cost(double value);
}
