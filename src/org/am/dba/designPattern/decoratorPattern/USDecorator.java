package org.am.dba.designPattern.decoratorPattern;

public abstract class USDecorator extends Decorator {
  
  Currency currency;

  
  public USDecorator() {
	this.currency = currency;
  }

  @Override
  public String getDescription() {
	return currency.getCurrencyDescription();
  }

}
