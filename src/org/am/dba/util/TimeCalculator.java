package org.am.dba.util;
 
public class TimeCalculator {
  public static void code(Runnable block){
	long start = System.nanoTime();
	try{
	  block.run();
	}finally{
	  long end = System.nanoTime();
	  System.out.println("Time Taken(s): "+(end-start)/1.0e9);
	}
  }
}
