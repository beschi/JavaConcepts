package org.am.dba.concurrent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ProcessBuilderExample {
  public static void main(String[] args) throws IOException, InterruptedException{
	ProcessBuilder pb = new ProcessBuilder("cmd.exe","/C","dir & echo example of & echo working dir");
	Process process = pb.start();
	int errorCode = process.waitFor(); // wait for the process to be completed.
	System.out.println(" Return code "+ errorCode);
	System.out.println(outputValue(process.getInputStream()));
  }
  
  private static String outputValue(InputStream in) throws IOException{
	StringBuilder sb = new StringBuilder();
	BufferedReader br = new BufferedReader(new InputStreamReader(in));
	String line = null;
	while((line = br.readLine()) != null){
	  sb.append(line).append(System.getProperty("line.separator"));
	}
	return sb.toString();
  }
}
