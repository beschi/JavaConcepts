package org.am.dba.jjwt;

import java.time.LocalDateTime;
import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtil {

  private String APP_NAME="MyApp";

  private String SECRET="itsmysecret";

  private int EXPIRES_IN=6000;

  public String getUsernameFromToken(String token) {
	String username;
	try {
	  final Claims claims = this.getClaimsFromToken(token);
	  username = claims.getSubject();
	} catch (Exception e) {
	  username = null;
	}
	return username;
  }

  public String generateToken(String username) {
	String jws = Jwts.builder().setIssuer(APP_NAME).setSubject(username)
		.setIssuedAt(generateCurrentDate())
		.setExpiration(generateExpirationDate())
		.signWith(SignatureAlgorithm.HS512, SECRET).compact();
	return jws;
  }

  private Claims getClaimsFromToken(String token) {
	Claims claims;
	try {
	  claims = Jwts.parser().setSigningKey(this.SECRET).parseClaimsJws(token)
		  .getBody();
	} catch (Exception e) {
	  claims = null;
	}
	System.out.println(claims);
	return claims;
  }

  private long getCurrentTimeMillis() {
	return  System.currentTimeMillis();
  }

  private Date generateCurrentDate() {
	return new Date(getCurrentTimeMillis());
  }

  private Date generateExpirationDate() {
	return new Date(getCurrentTimeMillis() + this.EXPIRES_IN * 1000);
  }
  
  public static void main(String[] args) {
	JWTUtil util = new JWTUtil();
	String myToken = util.generateToken("beschi");
	System.out.println(util.getUsernameFromToken(myToken));
  }
}
