package org.am.dba.lamda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class IteratorExample {
  
  public static void main(String[] args) {
	List<Integer> numbers = Arrays.asList(1,2,3,4,5);
	
	//External Iterator
	for(int i=0; i<numbers.size(); i++){
	  System.out.println(numbers.get(i));
	}
	
	//External forEach iterator
	for(Integer i : numbers){
	  System.out.println(i);
	}
	
	//Internal Iterators Java 8
	numbers.forEach(new Consumer<Integer>() {
	  @Override
	  public void accept(Integer t) {
		 System.out.println(t);
	  }
	});
	
	//Internal Iterator, removed the noisy anonymous class creation
	numbers.forEach((Integer t)->System.out.println(t));
	
	//Internal Iterator, removed type, as Java 8 has Type Inference only for Lambda Expressions
	numbers.forEach((t)->System.out.println(t));
	
	//Parenthesis is optional for single parameter lambda
	numbers.forEach(t->System.out.println(t));
	
	//Method reference in lambda, lamdas are simple, so keep that way
	numbers.forEach(System.out::println);
	
  }
}
