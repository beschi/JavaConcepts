package org.am.dba.lamda;

import java.util.Arrays;
import java.util.List;

public class FunctionCompositionExample {
  public static void main(String[] args){
	List<Integer> numbers = Arrays.asList(1,2,3,4,5);
	
	//Imperative Style
	//Given the numbers, double the even numbers and total.
	int result = 0;
	for(Integer e : numbers){
	  if(e % 2 == 0){
		result +=e*2;
	  }
	}
	System.out.println(result);
	
	
	//Functional Style
	System.out.println(numbers.stream()
		.filter(e -> e % 2 == 0)
		.map(e -> e * 2)
		.reduce(0, Integer::sum));
	
	//tuned
	System.out.println(
		numbers.stream()
			.filter(e -> e % 2 == 0)
			.mapToInt(e -> e * 2)
			.sum()
		);
	
  }
}
