package org.am.dba.lamda;

import java.util.Arrays;
import java.util.List;

public class MethodReference {
  public static void main(String[] args){
	List<Integer> numbers = Arrays.asList(1,2,3,4,5);
	
	numbers.forEach(e->System.out.println(e));
	
	//Method Reference should be used, only when the parameters are passed through, 
	//without any modification/ business logic.
	numbers.forEach(System.out::println);
	
	//Single Argument
	numbers.stream()
		   //.map(e->String.valueOf(e))
	       .map(String::valueOf) //STATIC method reference
	       .forEach(System.out::println);
	
	//Single Argument
	numbers.stream()
	       .map(String::valueOf)
	       //.map(e->e.toString())
	       .map(String::toString)//INSTANCE Method as well parameter to target
	       .forEach(System.out::println);
	
	//Two arguments to a method
	System.out.println(
		numbers.stream()
				//.reduce(0,(total,e)->Integer.sum(total, e))
				.reduce(0,Integer::sum)
		);
	
	//Two arguments but as a target
	System.out.println(
			numbers.stream()
					.map(String::valueOf)
					//.reduce("", (carry, str)->carry.concat(str))
					.reduce("", String::concat)
		);
  }
}
