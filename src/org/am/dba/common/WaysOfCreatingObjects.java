package org.am.dba.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class WaysOfCreatingObjects {
  public static void main(String[] args)
	  throws InstantiationException, IllegalAccessException,
	  NoSuchMethodException, SecurityException, IllegalArgumentException,
	  InvocationTargetException, CloneNotSupportedException,
	  FileNotFoundException, IOException, ClassNotFoundException {
	Employee emp1 = new Employee(); // 1 will/can call any constructor

	Employee emp2 = Employee.class.newInstance(); // 2 can call only no-argument
												  // constructor
	// Employee emp2 =
	// (Employee)Class.forName("org.am.dba.common.Employee").newInstance();

	Constructor<Employee> construct = Employee.class.getConstructor();
	Employee emp3 = construct.newInstance(); // 3 can call any constructor
	emp3.setName("Beschi");

	Constructor<?> cons[] = Employee.class.getDeclaredConstructors();
	System.out.println(cons.length);

	Employee emp4 = (Employee) emp3.clone(); // 4 object creation using cloning
	System.out.println(emp4.getName());
	System.out.println("Hashcode of Emp4 " + emp4.hashCode());

	ObjectOutputStream oos = new ObjectOutputStream(
		new FileOutputStream("emp5.obj"));
	oos.writeObject(emp4);

	ObjectInputStream ois = new ObjectInputStream(
		new FileInputStream("emp5.obj"));
	Employee emp5 = (Employee) ois.readObject();
	System.out.println(emp5.getName());
	System.out.println("Hashcode of Emp5 " + emp5.hashCode());
  }
}

class Employee implements Cloneable, Serializable {
  private static final long serialVersionUID = 1L;
  private String name;
  private int age;

  public String getName() {
	return name;
  }

  public void setName(String name) {
	this.name = name;
  }

  public int getAge() {
	return age;
  }

  public void setAge(int age) {
	this.age = age;
  }
 
  
  @Override
  public boolean equals(Object obj) {
	//Java 6 and <6
	if (this == obj)
	  return true;
	
	if (obj == null)
	  return false;
	
	if (getClass() != obj.getClass())
	  return false;
	
	Employee other = (Employee) obj;
	//Java 6
/*	return this.age == other.getAge() && 
		(this.name==other.name  || (this.name != null && this.name.equals(other.name)));*/
	//Java 7 and above
	
	return this.age == other.age &&
		Objects.equals(this.name, other.name);
  }

  @Override
  public int hashCode() {
	//Java 6 and <6
	/*final int prime = 31;
	int result = 1;
	result = prime * result + age;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;*/
	
	//Java 7 and above
	return Objects.hash(age, name);
  }

  public Employee() {
	System.out.println("Employee Constructor Called...");
  }

  private Employee(String name, int age) {
	super();
	this.name = name;
	this.age = age;
  }

  public Object clone() throws CloneNotSupportedException {
	return super.clone();
  }
}