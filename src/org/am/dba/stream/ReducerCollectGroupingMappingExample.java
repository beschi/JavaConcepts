package org.am.dba.stream;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.*;

public class ReducerCollectGroupingMappingExample {
  public static List<Person> createPeople(){
	return Arrays.asList(new Person("Beschi", "Male", 31), new Person("Antony", "Male", 30), new Person("Priya","Female",29), new Person("Priya","Female",32));
  }
  
  public static void main(String[] args){
	List<Person> people = createPeople();
	
	//Given a list of people, create a map where, name is the key, value is all the people with the given name
	//
	System.out.println(
	people.stream()
		  .collect(groupingBy(Person::getName)));
  }
}
