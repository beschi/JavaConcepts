package org.am.dba.stream;

public class Person {
  private String name;
  private String gender;
  private int age;
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  public int getAge() {
    return age;
  }
  public void setAge(int age) {
    this.age = age;
  }
  public Person(String name, String gender, int age) {
	super();
	this.name = name;
	this.gender = gender;
	this.age = age;
  }
  @Override
  public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Person [name=");
	builder.append(name);
	builder.append(", gender=");
	builder.append(gender);
	builder.append(", age=");
	builder.append(age);
	builder.append("]");
	return builder.toString();
  }
  
  
}

