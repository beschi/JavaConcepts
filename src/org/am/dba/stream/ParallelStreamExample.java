package org.am.dba.stream;

import java.util.Arrays;
import java.util.List;

import org.am.dba.util.TimeCalculator;

public class ParallelStreamExample {
  public static void main(String[] args){
 	List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
 	TimeCalculator.code(()->
 	System.out.println(
 		numbers.stream()
 			.filter(e -> e % 2 == 0)
 			.mapToInt(ParallelStreamExample::compute)
 			.sum()
 		)); 
 	
 	TimeCalculator.code(()->
 	System.out.println(
 		numbers.parallelStream()
 			.filter(e -> e % 2 == 0)
 			.mapToInt(ParallelStreamExample::compute)
 			.sum()
 		));
  }
  
  public static int compute(int number){
	try{
	  Thread.sleep(1000);
	}catch(InterruptedException ie){}
	return number * 2;
  }
}
