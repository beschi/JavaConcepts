package org.am.dba.stream;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.*;

public class ReducerCollectMapExample {
  public static List<Person> createPeople(){
	return Arrays.asList(new Person("Beschi", "Male", 31), new Person("Antony", "Male", 30), new Person("Priya","Female",29));
  }
  
  public static void main(String[] args){
	List<Person> people = createPeople();
	
	System.out.println(
	people.stream()
		  .collect(toMap(person->person.getName()+"-"+person.getAge(), person->person)));
  }
}
