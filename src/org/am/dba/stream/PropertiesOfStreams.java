package org.am.dba.stream;

import java.util.Arrays;
import java.util.List;

public class PropertiesOfStreams {
  public static void main(String[] args) {
	List<Integer> numbers = Arrays.asList(1,2,5,4,3,1,2,3,4,5);
	//Properties can be changed based on where it came from, here from list declaration
	//Or by during the computation
	
	numbers.stream()
			.filter(e->e%2==0)
			.sorted()
			.distinct()
			.forEach(System.out::println);
	//SIZED, ORDERD (As list is ordered), DISTINCT (by Computation) and SORTED (by Computation)
  }

}
