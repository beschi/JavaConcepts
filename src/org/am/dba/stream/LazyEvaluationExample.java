package org.am.dba.stream;

import java.util.Arrays;
import java.util.List;

public class LazyEvaluationExample {

  public static void main(String[] args) {
	List<Integer> numbers = Arrays.asList(1,2,3,5,4,6,7,8,9,10,11,12,13,14,14,15,17,16,18,19,20);
	//Given an orederd list, find the double of the first even number greater than 3.
	
	//Imperative style
	int result = 0;
	for(int e : numbers){
	  if(e>3 && e%2==0){
		result = e*2;
		break;
	  }
	}
	System.out.println(result); // 8 unit of opertions
	
	System.out.println( // 
		numbers.stream() //20 operations
	       	   .filter(e->e>3) //17 Operations
		       .filter(e->e%2==0) //9 Operations 
		       .map(e->e*2)
		       .findFirst() // 20+17+9 operations
		);
	
	System.out.println( // 
		numbers.stream() //20 operations
	       	   .filter(LazyEvaluationExample::isGT3) //17 Operations
		       .filter(LazyEvaluationExample::isEven) //9 Operations 
		       .map(LazyEvaluationExample::doubleIt)
		       .findFirst() // 20+17+9 operations
		);
  }
  
  public static boolean isGT3(int number){
	return number > 3;
  }
  
  public static boolean isEven(int number){
	return number %2 == 0;
  }
  
  public static int doubleIt(int number){
	return number * 2;
  }
}
