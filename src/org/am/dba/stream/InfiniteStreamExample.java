package org.am.dba.stream;

import java.util.stream.Stream;

public class InfiniteStreamExample {
  public static void main(String[] args) {
	
	//starts with 1, create series
	//1,2,3,4,...
	//IMMUTABILITY PAYS WAY TO ->NO SIDE EFFECT->LAZYNESS->INFINITE
	
	int k = 0;
	long n = 10;
	int index = k;
	int count = 0;
	int result = 0;
	while(count<n){
	  if(index % 2==0 && Math.sqrt(index)>20){
		result += index*2;
		  count++;
	  }
	  index++;
	}
	System.out.println(result);
	
	System.out.println(
			Stream.iterate(k, e->e+1) //unbounded, lazy
				  .filter(e->e%2==0) //unbounded, lazy
				  .filter(e->Math.sqrt(e)>20) //unbounded, lazy
				  .mapToInt(e->e*2) //unbounded, lazy
				  .limit(n) // sized, lazy
				  .sum() // min, max, etch terminal functions.
		);
  }
}
