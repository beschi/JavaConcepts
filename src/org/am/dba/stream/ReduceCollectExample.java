package org.am.dba.stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ReduceCollectExample {
  public static void main(String[] args){
	List<Integer> numbers = Arrays.asList(1,2,3,4,5,1,2,3,4,5);
	
	List<Double> evenNumbers = new ArrayList<>(); 
	//given the numbers, double the even numbers and add it to a list
	numbers.stream()
			.filter(e->e%2==0)
			.mapToDouble(e->e*2.0)
			.forEach(e->evenNumbers.add(e)); //DO NOT DO this, It is changing/Non Mutability is voilated. shared variable is changed.
	System.out.println(evenNumbers);
	
	List<Integer> evenNumbers1 =  numbers.stream()
			.filter(e->e%2==0)
			.map(e->e*2)
			.collect(toList());
	System.out.println(evenNumbers1);
	
	Set<Integer> evenNumbers2 = numbers.stream()
		.filter(e->e%2==0)
		.map(e->e*2)
		.collect(toSet());
	System.out.println(evenNumbers2);
  }
}
